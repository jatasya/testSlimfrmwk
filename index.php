<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Books</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>


<div class="container">

    <div class="book_list">Book list</div>
    <ul id="bookList"></ul>
    <input id="searchBook" type="text" placeholder="name Book">
    <button id="findAllBooks">Find ALL books</button>
</div>

<div class="wrap">

    <div class="author_list">Author list</div>
    <ul id="authorList"></ul>
    <input id="searchAuthor" type="text" placeholder="name Author">
    <button id="findAllAuthors">Find ALL authors</button>
</div>

<table id="books">

</table>

<script src="js/jquery-2.1.4.js"></script>
<script src="js/main.js"></script>
</body>
</html>

<?php
/**
 * Created by PhpStorm.
 * User: Evgeni
 * Date: 28.12.2015
 * Time: 23:31
 */