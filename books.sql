-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 29 2015 г., 02:53
-- Версия сервера: 5.5.45
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `books`
--

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `author_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`author_id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`author_id`, `first_name`, `last_name`, `rating`) VALUES
(1, 'Evgeni', 'Lanin', 4),
(2, 'Taicia', 'Lanina', 5),
(4, 'Evgeni', 'Muntyan', 3),
(5, 'Taicia', 'Muntyan', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `book`
--

INSERT INTO `book` (`book_id`, `name`, `rating`) VALUES
(1, 'Red head', 2),
(2, 'Blue head', 4),
(3, 'How to..', 2),
(4, 'Fish vs meat', 5),
(5, 'php develop', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `hooks`
--

CREATE TABLE IF NOT EXISTS `hooks` (
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  KEY `book_id` (`book_id`,`author_id`),
  KEY `book_id_2` (`book_id`,`author_id`),
  KEY `book_id_3` (`book_id`,`author_id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `hooks`
--

INSERT INTO `hooks` (`book_id`, `author_id`) VALUES
(1, 1),
(1, 4),
(1, 4),
(2, 5),
(2, 5),
(2, 5),
(2, 5),
(3, 4),
(3, 4),
(3, 4),
(3, 4);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `hooks`
--
ALTER TABLE `hooks`
  ADD CONSTRAINT `hooks_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`),
  ADD CONSTRAINT `hooks_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `authors` (`author_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
